
    <div class="card" style="height:200px">
      <h3 class="font-normal text-xl -ml-5 border-l-4 border-blue-100 pl-4 mb-3 py-4">
        <a href="{{$project->path()}}">{{$project->title}}</a>
      </h3>
      <div class="text-gray-500">{{str_limit($project->description, 100)}}</div>
    </div>