@extends('layouts.app')
@section('content')
<div class="card lg:w-1/2 lg:mx-auto">
<form action="{{$project->path()}}" method="POST">
    @csrf
    @method('PATCH')
    <h1 class="text-4xl mb-5 text-center">edit your project</h1>
    <div class="field mb-4">
      <label class="label block mb-1" for="title">title</label>
      <div>
        <input 
        class="w-full border border-gray-400 mb-4 rounded-lg" 
        type="text" 
        name="title" 
        placeholder="title"
      value="{{$project->title}}"
      required>
      </div>
    </div>
    <div class="field">
      <label class="label block mb-1" for="description">description</label>
      <div>
        <textarea 
        class="w-full border border-gray-400 mb-4 rounded-lg" 
        name="description" cols="30" rows="10"
        required
        >{{$project->description}}</textarea>
      </div>
    </div>
    <div>
      <div>
        <button class="button" type="submit">update</button>
      <a href="{{$project->path()}}">cancel</a>
      </div>
    </div>
    @if ($errors->any())
    <div class="field mt-6">
      @foreach ($errors->all() as $error)
      <li class="text-sm text-red-500 list-none">{{$error}}</li>
      @endforeach
    </div>
    @endif
  </form>
</div>
@endsection