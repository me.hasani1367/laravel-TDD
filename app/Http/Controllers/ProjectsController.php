<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index()
    {

        $projects = auth()->user()->projects;

        return view('projects.index')->with('projects', $projects);
    }

    public function show(Project $project)
    {

        $this->authorize('update', $project);

        return view('projects.show')->with('project', $project);
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store()
    {

        $project = auth()->user()->projects()->create($this->validateRequest());

        return redirect($project->path());
    }

    public function update(Project $project)
    {

        $this->authorize('update', $project);

        $project->update($this->validateRequest());

        return redirect($project->path());
    }

    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    protected function validateRequest()
    {
        return request()->validate([
            'title' => 'required',
            'description' => 'required',
            'notes' => 'min:3'
        ]);
    }
}
